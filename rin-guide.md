# CS.RIN.RU GUIDE BY MIDNYTE
 
- First things first we'll need to make an account so we can use download links.  

- Once you've done that we'll need to search for a game we want. I'm going to use Curse of the Dead Gods.

![image](/uploads/65688cd07b9417512695dd59656b8cb2/image.png)

- Upon searching it brings up a bunch of different threads so I will use CTRL + F and search "Curse of the Dead Gods" 

![image](/uploads/5004470fb4aa32f59e60d542369ec20d/image.png)

- There is a Steam Content Sharing (SCS) thread and an info thread, click on the info thread.

- In most cases, the info thread will have information on the game and possibly information about cracks and game files however, Curse of the Dead Gods does not. Therefore we will have to go to the Steam Content Sharing (SCS) page and download clean game files, then remove Steam DRM from it, removing Steam DRM is an easy task.

![image](/uploads/80bcf95b2a41ec8fdfc1afcc52e5c5dc/image.png)

- We'll go to the Steam Content Sharing (SCS) page to look for the latest files of the game and promptly download the latest build.

![image](/uploads/a60eff2f71db7024a16f498d176bd166/image.png)

- In most cases you can find a premade crack with no configuring needed and if there is one, you can stop here and apply the crack, however for this game, you'll need a fixed .exe and since .exe's change when a game is updated there will be no up-to-date crack posted on the thread.

- For this game we'll need to use a SteamStub Remover.

- To remove SteamStub we're going to be using a program called Steamless, it's very user friendly and has a GUI.

- To obtain it, please download the .zip file from here: <https://github.com/atom0s/Steamless/releases>

- Extract it, then launch it with steamless.exe and just select the game's .exe under file to unpack and click unpack file.

- An unpacked.exe will be in your game directory and you can now launch the game from the .exe! If it works, then you can skip the steps listed below, otherwise keep following

- In most cases you will need to apply a Steam Emulator (which you can read about on cs.rin.ru)

- Here's a link to Goldberg's Steam emu, please read the "How to Use" section carefully: <https://gitlab.com/Mr_Goldberg/goldberg_emulator>

- Basically, all you need to do is replace the steam_api.dll and steam_api64.dll from the emulator to the game files.

- If you have any more questions about cs.rin.ru feel free to ask on cs.rin.ru as the community is very friendly. 
 
Happy Pirating!

Here's a link to the General FAQ of cs.rin.ru: <https://cs.rin.ru/forum/viewtopic.php?p=1926325#p1926325>
